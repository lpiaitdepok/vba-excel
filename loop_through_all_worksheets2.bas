Sub loop_through_all_worksheets2()

Dim i As Integer
Dim ws_num As Integer

Dim starting_ws As Worksheet
Set starting_ws = ActiveSheet 'remember which worksheet is active in the beginning
ws_num = ThisWorkbook.Worksheets.count

For i = 1 To ws_num
    ThisWorkbook.Worksheets(i).Activate
    'do whatever you need
    ThisWorkbook.Worksheets(i).Cells(1, 1) = 1  'this sets cell A1 of each sheet to "1"
Next

starting_ws.Activate 'activate the worksheet that was originally active

End Sub