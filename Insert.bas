Range("A1").Insert(Shift, CopyOrigin)

'Shift

'    xlShiftDown or -4121: Shifts cells down.

'    xlShiftToRight or -4161: Shifts cells to the right.

'Default: Excel decides based on the range's shape.
'CopyOrigin.

'    Description: Specifies from where (the origin) is the format for the cells in the newly inserted row copied.

'    Optional/Required: Optional.

'    Data type: Variant.

'    Values: A constant from the xlInsertFormatOrigin Enumeration:

'        xlFormatFromLeftOrAbove or 0: Newly-inserted cells take formatting from cells above or to the left.

'        xlFormatFromRightOrBelow or 1: Newly-inserted cells take formatting from cells below or to the right.

'    Default: xlFormatFromLeftOrAbove or 0.