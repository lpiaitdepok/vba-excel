'https://www.excel-easy.com/vba/

'current region
' a range bounded by any combination of blank rows and blank columns.
Range("cell address").CurrentRegion.Select

'all cells
' all cells in a worksheet
Cells.Select
' or
Columns.Select
' or
Rows.Select

' select cell in range
Dim cell As Range, rng As Range
Set rng = Range("cell address").CurrentRegion
For Each cell In rng
    If  Then 
Next cell

' entire second columns
Columns(2).Select
' To select multiple columns
Columns("B:E").Select

' entire seventh rows
Rows(2).Select
' To select multiple rows
Rows("5:7").Select

' return a single value
Cells(5, 2).Row
Cells(5, 2).Column

'  selects the entire row of the active cell.
ActiveCell.EntireRow.Select

' To select the last entry in a column
Range("A5").End(xlDown).Select

'  To select the range from cell A5 to the last entry in the column
Range(Range("A5"), Range("A5").End(xlDown)).Select

' To select first range of areas of rangeToUse
rangeToUse.Areas(1).Select